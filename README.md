## Notas - app
Pequeña aplicación con la que podrás probar como se crea un CRUD a la vez que puedes crear notas recordatorias (Post-It)

## En que Consiste?
Aplicación creada haciendo uso de Node.js Express.js MongoDB (Mogoose) y el manejador de template Handlebars. (Curso impartido por: [](https://www.youtube.com/watch?v=-bI0diefasA) Fazt Code con algunos cambios y mejoras debido a la actualizacion de mongoDB a 4.4.2 y Node,js a 14.15.1)

## En gitlab:
[](https://gitlab.com/devdoblea/mi-mongo-node-app)

## Desarrollador
Angel Emiro Antunez Villasmil [here](https://elcuartodeangel.wordpress.com)
Venezuela


## Tecnologias:
Node.js: 14.15.1
Express.js: 4.x API [](https://expressjs.com/en/4x/api.html#app.engine)
MongoDB: 4.4.2 (Mogoose)
HandleBars: 5.0 [](https://github.com/handlebars-lang/handlebars.js)
HTML5
CSS3
Boostrap 4
Jquery 3


Curso realizado desde
[](https://www.youtube.com/watch?v=-bI0diefasA)
Fast Code Canal de Youtube