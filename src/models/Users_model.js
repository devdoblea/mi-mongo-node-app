const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
	username: { type: String, required: true },
	email: { type: String, required: true },
	password: { type: String, required: true },
	date: { type: Date, default: Date.now }
});

// Para encriptar la contraseña usando Bcrypt
UserSchema.methods.encryptPassword = async (password) => {
	const salt = await bcrypt.genSalt(10);
	const hash = bcrypt.hash(password, salt)
	return hash;
};

/* Para poder comparar las contraseñas se usa este metodo
* pero en vez de usar el metodo flecha vamos a usar una
* funcion para poder tener acceso a las variables con
* las que se está trabajando en este modelo */
UserSchema.methods.matchPassword = async function(password) {
	return await bcrypt.compare(password, this.password);
};
// Esta es otra forma de comprarar que consegui en la red
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('Users', UserSchema);