const express = require('express');
const router = express.Router();

// Incluir el Modelo para poder trabajar con sus datos
const Notes = require('../models/Notes_model');

// Para autenticar al usuario y permitir que trabaje con el html dado
const { isAuthenticated } = require('../helpers/auth');

// Ver Form Agregar
router.get('/notes/add', isAuthenticated, (req, res) => {
	//
	res.render('notes/new-note');
});

// Ver Form Editar
router.get('/notes/edit/:id', isAuthenticated, async (req, res) => {
	// Ubicar los datos de la nota segun el id
	const nota = await Notes.findById(req.params.id);
	res.render('notes/edit-note', { nota });
});

// Insertar una Nota
router.post('/notes/new-post', isAuthenticated, async (req, res) => {
	const { titulo, textnota } = req.body;
	const errors = [];
	if(!titulo) {
		errors.push({text: 'Por Favor, escriba el titulo de la nota'});
	}
	if(!textnota) {
		errors.push({text: 'Por Favor, escriba el texto de la nota'});
	}
	if( errors.length > 0 ) {
		res.render('notes/new-note', {
			errors,
			titulo,
			textnota
		});
	} else {
		// res.render('notes/new-note', { resultado Ok});
		//res.send('Resultado Ok');
		// Almacenarlo en la BD
		const newNote = new Notes({ titulo, textnota });
		/* con esta linea le agrego el id del usuario que
		 * está creando la nota al registro para poder luego
		 * discriminar cual nota es de cual usuario */
		newNote.user = req.user.id;
		await newNote.save();
		req.flash('success_msg', 'Nota agregada Exitósamente');
		res.redirect('/notes');
	}
});

// Editar la Nota especificada
router.put('/notes/edit-post/:id', isAuthenticated, async (req, res) => {
	const { titulo, textnota } = req.body;
	const errors = [];
	if(!titulo) {
		errors.push({text: 'Por Favor, escriba el titulo de la nota'});
	}
	if(!textnota) {
		errors.push({text: 'Por Favor, escriba el texto de la nota'});
	}
	if( errors.length > 0 ) {
		res.render('notes/edit-note', {
			errors,
			titulo,
			textnota
		});
	} else {
		// Modificarlo en la BD
		await Notes.findByIdAndUpdate(req.params.id, { titulo, textnota });
		req.flash('success_msg', 'Nota Actualizada.!');
		res.redirect('/notes');
	}
});

// borrar la Nota especificada
router.delete('/notes/del-post/:id', isAuthenticated, async (req, res) => {
 	// Borrarlo de la BD
	await Notes.findByIdAndDelete(req.params.id);
	req.flash('success_msg', 'Nota Eliminada.!');
	res.redirect('/notes');
});

// Mostrar todas las Notas
router.get('/notes', isAuthenticated, async (req, res) => {
	const notes = await Notes.find({ user: req.user.id }).sort({date: 'desc'});
	res.render('notes/index-notes', { notes });
});

module.exports = router