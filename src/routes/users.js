const express = require('express');
const router = express.Router();

// Incluir el Modelo para poder trabajar con sus datos
const Users = require('../models/Users_model');

// Para autenticar al usuario y permitir que trabaje con el html dado
const { isAuthenticated } = require('../helpers/auth');

// Ir a la pagina de Registro cuando inicia el sistema
router.get('/users/signup', isAuthenticated, (req, res) => {
	res.render('users/add-user');
});

// Insertar un Usuario desde el inicio del sistema
router.post('/users/signup', isAuthenticated, async (req, res) => {
	//const { username, email, password, confirm_password } = req.body;
	// recibiendo lo posteado desde el form
	const posteos = req.body;
	// Mostrar las variables que llegan en formato JSON
	// console.log(JSON.stringify(posteos, undefined, 4));

	/* Para poder usar lo posteado, por ejemplo al momento de devolverlo
	 * al formulario o en caso de enviarlas al modelo a traves del objeto Users
	 * voy a convertir lo posteado a variables, permitiendo asi que sean
	 * llamadas desde cualquier parte del subsiguiente codigo,
	 */
	var name = posteos.username;
	var emai = posteos.email;
	var pass = posteos.password;
	var cpas = posteos.confirm_password;
	// Evaluando errores
	const errors = [];
	if( !posteos.username ) { // aqui pude haber usado las "var" pero preferí probar asi
		//
		errors.push({text: 'Por Favor, escriba el Nombre de Usuario'});
	}
	if( !posteos.email ) {
		//
		errors.push({text: 'Por Favor, escriba el Correo Electrónico'});
	}
	if( posteos.password < 8) {
		//
		errors.push({text: 'La contraseña debe contener al menos 8 caracteres'});
	}
	if( posteos.password != posteos.confirm_password) {
		//
		errors.push({text: 'Las contraseñas deben coincidir'});
	}
	if( errors.length > 0 ) {
		// redirijo hacia el formulario para que el usuario se de cuenta que hubo errores
		res.render('users/signup', { errors, name, emai, pass, cpas });
	} else {
		// Si todo esta bien entonces preparo todo para guardar en la BD
		// verifico si existe el email para que no existan emails duplicados.
		const emailUser = await Users.findOne({email: posteos.email}).exec(); // Consulta al modelo
		//console.log('condicion findOne: ', emailUser._conditions);
		//console.log('condicion findOne: ', emailUser);
		// SI existe el email en la BD entonces que el usuario escriba otro. Le puedo avisar de 2 formas.
		if( emailUser ) {
			// Forma 1
			//req.flash('error_msg', 'Correo ya está Registrado Anteriormente');
			//res.redirect('/');
			// Forma 2
			errors.push({text: 'Correo ya está Registrado Anteriormente'});
			res.render('users/signup', { errors, name, emai, pass, cpas });
		} else {
			// Mostrar las variables que llegan en formato JSON
			//console.log(JSON.stringify(posteos, undefined, 4));
			//res.send('Resultado Ok<br>'+JSON.stringify(posteos, undefined, 4));
			// Almacenarlo en la BD pero usando mongoose como dice en su pagina: https://mongoosejs.com/
			// objeto({ clave: valor })
			const newUser = await new Users({ username: name, email: emai, password: pass });
			// Encripto la constraseña y a la funcion le envio como parametro la variable "pass"
			newUser.password = await newUser.encryptPassword(pass);
			// SAlvo con una funcion asincrona para que nodejs espere a que se grabe para avisar si hubo error
			await newUser.save()
			             .then(() => console.log('Registro Salvado'))
			             .catch(err => console.error('Error Salvando: '+err));
			// Finalmente redirijo al formulario signin para que se loguee con el nuevo usuario que se creó
			// Ojo que aqui se pudo meter esto en el then() del save() pero estoy probando asi...
			req.flash('success_msg', 'Usuario agregado Exitósamente');
			res.redirect('/users');
		}
	}
});

// Ver Form Editar Usuario
router.get('/users/edit-user/:id', isAuthenticated, async (req, res) => {
	// Ubicar los datos de la nota segun el id
	const nota = await Notes.findById(req.params.id);
	res.render('users/edit-user', { nota });
});

// borrar un Usuario especifico
router.delete('/users/del-user/:id', isAuthenticated, async (req, res) => {
 	// Borrarlo de la BD
	await Users.findByIdAndDelete(req.params.id);
	req.flash('success_msg', 'Usuario Eliminado.!');
	res.redirect('/users');
});

// Ver Todos los Usuarios
router.get('/users', isAuthenticated, async (req, res) => {
	const users = await Users.find().sort({date: 'desc'});
	res.render('users/index-users', { users });
});


module.exports = router