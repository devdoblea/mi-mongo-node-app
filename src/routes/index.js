const express = require('express');
const router = express.Router();

// Incluir el Modelo para poder trabajar con sus datos
const Users = require('../models/Users_model');

// Incluir la libreria Passport para manejar variables de sesion en el login
const passport = require('passport');

// Ir a la pagina de Registro cuando inicia el sistema
router.get('/', (req, res) => {
	res.render('users/signin', { layout: 'login' });
});

// Ir a la pagina de Registro cuando inicia el sistema
router.get('/signup', (req, res) => {
	res.render('users/signup', { layout: 'login' });
});

// Ir a la pagina de logueo
router.get('/signin', (req, res) => {
	res.render('users/signin', { layout: 'login' });
});

// Insertar un Usuario desde el inicio del sistema
router.post('/signup', async (req, res) => {
	//const { username, email, password, confirm_password } = req.body;
	// recibiendo lo posteado desde el form
	const posteos = req.body;
	// Mostrar las variables que llegan en formato JSON
	// console.log(JSON.stringify(posteos, undefined, 4));

	/* Para poder usar lo posteado, por ejemplo al momento de devolverlo
	 * al formulario o en caso de enviarlas al modelo a traves del objeto Users
	 * voy a convertir lo posteado a variables, permitiendo asi que sean
	 * llamadas desde cualquier parte del subsiguiente codigo,
	 */
	var name = posteos.username;
	var emai = posteos.email;
	var pass = posteos.password;
	var cpas = posteos.confirm_password;
	// Evaluando errores
	const errors = [];
	if( !posteos.username ) { // aqui pude haber usado las "var" pero preferí probar asi
		//
		errors.push({text: 'Por Favor, escriba el Nombre de Usuario'});
	}
	if( !posteos.email ) {
		//
		errors.push({text: 'Por Favor, escriba el Correo Electrónico'});
	}
	if( posteos.password < 8) {
		//
		errors.push({text: 'La contraseña debe contener al menos 8 caracteres'});
	}
	if( posteos.password != posteos.confirm_password) {
		//
		errors.push({text: 'Las contraseñas deben coincidir'});
	}
	if( errors.length > 0 ) {
		// redirijo hacia el formulario para que el usuario se de cuenta que hubo errores
		res.render('users/signup', { layout: 'login', errors, name, emai, pass, cpas });
	} else {
		// Si todo esta bien entonces preparo todo para guardar en la BD
		// verifico si existe el email para que no existan emails duplicados.
		const emailUser = await Users.findOne({email: posteos.email}).exec(); // Consulta al modelo
		//console.log('condicion findOne: ', emailUser._conditions);
		//console.log('condicion findOne: ', emailUser);
		// SI existe el email en la BD entonces que el usuario escriba otro. Le puedo avisar de 2 formas.
		if( emailUser ) {
			// Forma 1
			//req.flash('error_msg', 'Correo ya está Registrado Anteriormente');
			//res.redirect('/');
			// Forma 2
			errors.push({text: 'Correo ya está Registrado Anteriormente'});
			res.render('users/signup', { layout: 'login', errors, name, emai, pass, cpas });
		} else {
			// Mostrar las variables que llegan en formato JSON
			//console.log(JSON.stringify(posteos, undefined, 4));
			//res.send('Resultado Ok<br>'+JSON.stringify(posteos, undefined, 4));
			// Almacenarlo en la BD pero usando mongoose como dice en su pagina: https://mongoosejs.com/
			// objeto({ clave: valor })
			const newUser = await new Users({ username: name, email: emai, password: pass });
			// Encripto la constraseña y a la funcion le envio como parametro la variable "pass"
			newUser.password = await newUser.encryptPassword(pass);
			// SAlvo con una funcion asincrona para que nodejs espere a que se grabe para avisar si hubo error
			await newUser.save()
			             .then(() => console.log('Registro Salvado'))
			             .catch(err => console.error('Error Salvando: '+err));
			// Finalmente redirijo al formulario signin para que se loguee con el nuevo usuario que se creó
			// Ojo que aqui se pudo meter esto en el then() del save() pero estoy probando asi...
			req.flash('success_msg', 'Usuario agregado Exitósamente');
			res.redirect('/signin');
		}
	}
});

// crear las variables de sesion al logearse
router.post('/signin', passport.authenticate('local', {
	successRedirect: '/notes',
	failureRedirect: '/signin',
	failureFlash: true
}));

// loguearse
router.post('/signin', async (req, res) => {
	//const { email, password } = req.body;
	const posteos = req.body;
	/* Para poder usar lo posteado, por ejemplo al momento de devolverlo
	 * al formulario o en caso de enviarlas al modelo a traves del objeto Users
	 * voy a convertir lo posteado a variables, permitiendo asi que sean
	 * llamadas desde cualquier parte del subsiguiente codigo,
	 */
	var emai = posteos.email;
	var pass = posteos.password;
	// Evaluando errores
	const errors = [];

	if(!emai) {
		errors.push({text: 'Por Favor, escriba el Correo Electrónico'});
	}
	if(!pass) {
		errors.push({text: 'Por Favor, escriba el Password'});
	}
	if( errors.length > 0 ) {
		res.render('users/signin', { layout: 'login', errors, emai, pass });
	} else {
		// Comparo los datos con lo registrado en la BD
		// verifico si existe el email para verificar si esta registrado.
		const emailUser = await Users.findOne({ email: emai }).exec(); // Consulta al modelo
		// SI no existe el email en la BD entonces es que el usuario no esta reg o se equivoco. Le aviso
		if( ! emailUser ) {
			// Le aviso al usuario que no se consiguió el email
			errors.push({text: 'Correo NO está Registrado. Verifique'});
			res.render('/signin', { layout: 'login', errors, emai, pass });
		}
		// Consulto con los datos recibidos via post
		// Primero verifico el usuario usando una funcion y dentro de esa funcion, verifico el password
		await Users.findOne({ email: emai }, function(err, user) {
      if (err) {
      	// Le aviso al usuario que no se consiguió el email
				errors.push({text: 'Error buscando el Usuario. Verifique'});
				res.render('/signin', { layout: 'login', errors, emai, pass });
      	//throw err
      };
      // probando si es igual el password
      // este metodo lo puedo utilizar porque lo he creado en el Modelo
      user.comparePassword(pass, function(err, isMatch) {
        if (err) {
	        // Le aviso al usuario que no se consiguió el email
					errors.push({text: 'Error comparando password. Verifique'});
					res.render('/signin', { layout: 'login', errors, emai, pass });
	      	//throw err
      	}
        console.log(pass, isMatch); // -> Password123: true
        // probando si ES IGUAL el password
        if( isMatch === true ) {
        	req.flash('success_msg', 'Bienvenid@.!');
					res.redirect('/main');
        } else if( isMatch === false ) {
        	// si NO ES IGUAL el password
        	req.flash('error_msg', 'No es igual el Password. Problemas al loguearse.!');
					res.redirect('/signin');
        }
      });
    });
	}
});

// Si ya está logueado entonces ir al dashboard
router.get('/main', (req, res) => {
	res.render('index');
});

// Si esta en el inicio, ir a la pagina "Acerca de"
router.get('/about', (req, res) => {
	res.render('about');
});

// Si esta en el inicio, ir a la pagina "Acerca de"
router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/');
});

module.exports = router