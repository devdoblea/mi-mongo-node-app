const express = require('express');
const path = require('path'); // para inocular la ruta del servidor
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash'); // Para enviar mensajes via session
const passport = require('passport'); // Para variables de sesion y logueo

/* INICIALIZACION */
const app = express();
require('./database'); // Para conectarme a la Mongo-DB
require('./config/passport'); //para importar la libreria passport

/* SETTINGS */
app.set('env', 'development');
app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views')); // ruta del servidor + ruta vistas
app.engine('hbs', exphbs({
	defaultlayout: 'main',
	layoutsDir: path.join(app.get('views'), 'layouts'),
	partialsDir: path.join(app.get('views'), 'partials'),
	extname: 'hbs',
	runtimeOptions: {
      allowProtoPropertiesByDefault: true,
      allowProtoMethodsByDefault: true,
  },
  helpers: {
		json: function (context) {
			return JSON.stringify(context);
		},
		split: function (str) {
			var t = str.split(" ");
  		return t[0];
		}
	},
}));
app.set('view engine', 'hbs');

/* ERROR HANDLERS. Development error handler will print stacktrace */
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
    message: err.message,
    error: err
    });
  });
}

/* MIDDLEWARE */
// para codificar el texto en HTML y que sea legible
app.use(express.urlencoded({extended: false}));
// Para reescribir los metodos de posteos desde los form de las vistas
app.use(methodOverride('_method'));
// Para las variables de sesion
app.use(session({
	secret: 'mysecretapp',
	resave: true,
	saveUninitialized: true,
	cookie: {
		maxAge: 3600000,
		expires: new Date(Date.now() + 3600000),
		sameSite: 'lax',
	}
}));
// usar passport despues de la libreria session
app.use(passport.initialize());
app.use(passport.session());
// para configurar el envio de mensajes entre las vistas
app.use(flash());


/* GLOBAL VARIABLES */
app.use((req, res, next) => {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	// manejar errores q vienen desde la libreria passport
	res.locals.error = req.flash('error');
	// Nombre del Usuario para compartirlo a travez de la aplicacion
	res.locals.user = req.user || null;
	// instanciar variables de sesion para poder compartirlas via flash a todas las vistas
	res.locals.session = req.session;
	next();
});

/* ROUTES */
app.use(require('./routes/index'));
app.use(require('./routes/notes'));
app.use(require('./routes/users'));

/* STATIC FILES */
app.use(express.static(path.join(__dirname, 'public')));

/* SERVER IS LISTENING */
app.listen(app.get('port'), (err) => {
	if (err) console.log("Error in server setup: "+err)
	console.log('Server on port', app.get('port'));
});