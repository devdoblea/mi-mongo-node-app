const helpers = {};

// Funcion para verificar si el usuario está logeado y si no, enviarlo a que se logee
helpers.isAuthenticated = (req, res, next) => {
	if(req.isAuthenticated()) {
		return next();
	}
	req.flash('error_msg', 'Autentícate antes de continuar');
	res.redirect('/signin');
};

module.exports = helpers;