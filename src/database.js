const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/notes-db-app',{
	useCreateIndex: true,
	useNewUrlParser: true,
	useFindAndModify: false,
	useUnifiedTopology: true
})
 .then(db => console.log('BD conectada con exito'))
 .catch(err => console.error('Error al Conectar: '+err));