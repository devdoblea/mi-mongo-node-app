const passport = require('passport');
// De la libreria passport solo extraigo la estrategia de logeo
const LocalStrategy = require('passport-local').Strategy;

// Cargo el modelo de Usuarios
const Users = require('../models/Users_model');

passport.use(new LocalStrategy({
	usernameField: 'email',
}, async (email, password, done) => {
	// Aqui la instanciacion del objeto a buscar
	const user = await Users.findOne({ email: email }).exec(); // Consulta al modelo
	if(!user){
		return done(null, false, { message: 'Usuario no existe'});
	} else {
		/* No se usa el Objeto del Modelo sino el Objeto Instanciado
		 * para que lo busque segùn el valor que se haya intentado buscar */
		const match = await user.matchPassword(password);
		if(match){
			return done(null, user);
		} else {
			return done(null, false, { message: 'Password Incorrecto'});
		}
	}
}));

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	Users.findById(id, (err, user) => {
		done(err, user);
	}).exec();
});